#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include "sockets.h"

void print_help(char* prog)
{
	printf("Using: %s HOST_NAME PORT\n", prog);
}

size_t recv_msg(SOCKET server_socket, unsigned char* command, char* msg, size_t max_msg_len, SOCKADDR_IN* addr, socklen_t* addr_len)
{
	char* buf = malloc(max_msg_len+1);
	size_t msg_len = recvfrom(server_socket, buf, max_msg_len+1, MSG_WAITALL, (struct sockaddr*)addr, addr_len);
	*command = buf[0];
	memcpy(msg, buf+1, msg_len);
	free(buf);
	return msg_len;
}

void send_msg(SOCKET fd, unsigned char command, const void *msg, size_t msg_len, const SOCKADDR_IN* addr, socklen_t addr_len)
{
	char* buf = malloc(msg_len + 1);
	buf[0] = command;
	memcpy(buf+1, msg, msg_len);

	sendto(fd, buf, msg_len+1, MSG_CONFIRM, (const struct sockaddr*)addr, addr_len);
	free(buf);
}

void send_hello(SOCKET fd, const SOCKADDR_IN* addr, socklen_t addr_len)
{
	const char* hello_msg = "hello";
	send_msg(fd, 0, hello_msg, strlen(hello_msg), addr, addr_len);
}

void send_byebye(int fd, const SOCKADDR_IN* addr, socklen_t addr_len)
{
	const char* hello_msg = "byebye";
	send_msg(fd, 1, hello_msg, strlen(hello_msg), addr, addr_len);
}

int main(int argc, char** argv)
{
	if (argc != 3)
	{
		print_help(argv[0]);
		return -1;
	}

	char* host = argv[1];
	int port = atoi(argv[2]);
	printf("[ INFO ] Start server on port %d\n", port);


	printf("[ DEBUG ] Creating socket\n");
	SOCKET client_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (client_socket == INVALID_SOCKET)
	{
		printf("Error while create socket\n");
		return -2;
	}

	SOCKADDR_IN server_addr;
	bzero((char*)&server_addr, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(port);

	// Connect socket to port
	if (inet_pton(AF_INET, host, &server_addr.sin_addr) < 0)
	{
		printf("[ ERROR ] Error while converting host address (%s) to struct in_addr\n", host);
		return -3;
	}

	// Connect to server
	printf("[ DEBUG ] Connecting to server %s:%d\n", host, port);

	send_hello(client_socket, (const SOCKADDR_IN*)&server_addr, sizeof(server_addr));

	while (1)
	{
		printf(">>> ");

		char* msg;
		size_t msg_len = 0;

		int read_len = getline(&msg, &msg_len, stdin);
		if (read_len < 0) break;

		if (read_len > 0 && msg[read_len-1] == '\n')
		{
			msg[read_len-1] = 0;
			read_len -= 1;
		}
		if (read_len == 0) continue;

		send_msg(client_socket, 255, msg, read_len, &server_addr, sizeof(server_addr));
	}
	printf("\n");

	send_byebye(client_socket, (const SOCKADDR_IN*)&server_addr, sizeof(server_addr));

	close(client_socket);

	return 0;
}
