#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#include "sockets.h"

#define MAX_MSG_LEN 1024


void print_help(char* prog)
{
	printf("Using: %s PORT\n", prog);
}

// Convert a struct sockaddr address to a string, IPv4 and IPv6:

char* get_ip_str(const struct sockaddr *sa, char *s, size_t maxlen)
{
    switch(sa->sa_family) {
        case AF_INET:
            inet_ntop(AF_INET, &(((struct sockaddr_in *)sa)->sin_addr),
                    s, maxlen);
            break;

        case AF_INET6:
            inet_ntop(AF_INET6, &(((struct sockaddr_in6 *)sa)->sin6_addr),
                    s, maxlen);
            break;

        default:
            strncpy(s, "Unknown AF", maxlen);
            return NULL;
    }

    return s;
}

size_t recv_msg(SOCKET server_socket, unsigned char* command, char* msg, size_t max_msg_len, SOCKADDR_IN* addr, socklen_t* addr_len)
{
	char* buf = malloc(max_msg_len+1);
	size_t msg_len = recvfrom(server_socket, buf, max_msg_len+1, MSG_WAITALL, (struct sockaddr*)addr, addr_len);
	*command = buf[0];
	memcpy(msg, buf+1, msg_len);
	free(buf);
	return msg_len;
}

void send_msg(SOCKET fd, unsigned char command, const void *msg, size_t msg_len, const SOCKADDR_IN* addr, socklen_t addr_len)
{
	char* buf = malloc(msg_len + 1);
	buf[0] = command;
	memcpy(buf+1, msg, msg_len);

	sendto(fd, buf, msg_len+1, MSG_CONFIRM, (const struct sockaddr*)addr, addr_len);
	free(buf);
}

int process_client(SOCKET server_socket)
{
	size_t client_addrs_size = 0;
	SOCKADDR_IN client_addrs[32];

	while (1)
	{
		SOCKADDR_IN client_addr;
		bzero((char*)&client_addr, sizeof(client_addr));

		socklen_t client_addr_len = sizeof(client_addr);

		unsigned char command;
		char* msg = malloc(MAX_MSG_LEN+1);
		int msg_len = recv_msg(server_socket, &command, msg, MAX_MSG_LEN, &client_addr, &client_addr_len);
		msg[msg_len] = 0;


		char ip_str[18];
		get_ip_str((const struct sockaddr *)&client_addr, ip_str, 18);

		printf("[ DEBUG ] Recive from client (%s:%d) msg (%d): %s\n", ip_str, client_addr.sin_port, command, msg);


		if (command == 0) // hello
		{
			printf("[ DEBUG ] Client (%s:%d) connected\n", ip_str, client_addr.sin_port);
			memcpy(client_addrs + client_addrs_size, &client_addr, sizeof(client_addr));
			client_addrs_size++;
		}
		else if (command == 1) // byebye
		{
			printf("[ DEBUG ] Client (%s:%d) disconnected\n", ip_str, client_addr.sin_port);
		}
		else
		{
			for (size_t i = 0; i < client_addrs_size; i++)
			{
				SOCKADDR_IN* client_addr = client_addrs + i;

				char ip_str[18];
				get_ip_str((const struct sockaddr *)client_addr, ip_str, 18);

				printf("[ DEBUG ] Sending msg '%s' to client (%s:%d)\n", msg, ip_str, client_addr->sin_port);
				send_msg(server_socket, 255, msg, msg_len, client_addrs + i, sizeof(client_addrs[i]));
			}
		}

		free(msg);
	}

	return 0;
}

int main(int argc, char** argv)
{
	if (argc != 2)
	{
		print_help(argv[0]);
		return -1;
	}

	int port = atoi(argv[1]);
	printf("[ INFO ] Start server on port %d\n", port);


	printf("[ DEBUG ] Creating socket\n");
	SOCKET server_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (server_socket == INVALID_SOCKET)
	{
		printf("Error while create socket\n");
		return -2;
	}

	// Allow to reuse same port
	printf("[ DEBUG ] Allowing to reuse port\n");
	int yes = 1;
	setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, (const char*)&yes, sizeof(int));

	SOCKADDR_IN server_addr;
	bzero((char*)&server_addr, sizeof(server_addr));
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = INADDR_ANY;
	server_addr.sin_port = htons(port);

	// Connect socket to port
	printf("[ DEBUG ] Binding socket to port %d\n", port);
	if (bind(server_socket, (SOCKADDR*)&server_addr, sizeof(server_addr)) < 0)
	{
		printf("Error while binding socket");
		return -3;
	}

	printf("[ DEBUG ] Ready for connection\n");

	// Accept connection
	process_client(server_socket);
	close(server_socket);

	return 0;
}
